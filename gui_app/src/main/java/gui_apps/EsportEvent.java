package gui_apps;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EsportEvent {
	
	@Id
	private int id;
	
	
	private String name;
	private String game;
	private String place;
	private int seats;
	private String dateevent;
	
	
	
	/*java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
	String currentTime = sdf.format(dt);*/
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public String getDateevent() {
		return dateevent;
	}
	public void setDateevent(String dateevent) {
		this.dateevent = dateevent;
	}

	
}
